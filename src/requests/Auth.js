import axios from 'axios';

export const login = async ({ email, password }) => {
  const { data } = await axios
    .post('/login', {
      email: email.trim(),
      password,
    });
  return data;
};

export const logout = async () => {
  const { data } = await axios
    .post('/logout');
  return data;
};

export const confirmEmail = async (id) => {
  console.log('id from auth', id);
  // console.log('token from auth', token);
  const { data } = await axios
    .put(`/confirm-account/${id}`);
  // const { data } = await axios
  //   .put(`/confirm-account/${id}`, {
  //     headers: { Authorization: `Bearer ${token}` },
  //   });
  return data;
};
