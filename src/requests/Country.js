import axios from 'axios';

export const select = async () => {
  const { data } = await axios.get('/countries/select');
  return data;
};
