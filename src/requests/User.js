import axios from 'axios';

export const index = async (pagination) => {
  const { data } = await axios.get('/projects', {
    params: {
      per_page: pagination.per_page,
      page: pagination.page,
    },
  });
  return data;
};

export const store = async (form) => {
  const { data } = await axios.post('/register', form);
  return data;
};

export const show = async (id) => {
  const { data } = await axios.get(`/projects/${id}`);
  return data;
};
