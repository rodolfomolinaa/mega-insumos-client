import 'core-js/es6/promise';
import 'core-js/es6/string';
import 'core-js/es7/array';
// import cssVars from 'css-vars-ponyfill'
import Vue from 'vue';
import moment from 'moment';
import 'moment-timezone';
import BootstrapVue from 'bootstrap-vue';
import { ValidationProvider, ValidationObserver } from 'vee-validate';
import App from './App.vue';
import './plugins';
import router from './router';
import store from './store';

// todo
// cssVars()

Vue.use(BootstrapVue);
Vue.component('validation-provider', ValidationProvider);
Vue.component('validation-observer', ValidationObserver);

window.moment = moment;

Vue.filter('moment', (date, format) => {
  if (date) {
    return moment.tz(date, 'UTC').local().format(format);
  }
  return '';
});


/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: {
    App,
  },
  template: '<App/>',
});
