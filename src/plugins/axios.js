import axios from 'axios';
import store from '../store';

axios.interceptors.request.use((config) => {
  config.baseURL = 'http://localhost:8000/api';
  const token = store.state.auth.access_token;
  if (token) {
    config.headers.common.Authorization = `Bearer ${token}`;
  }
  return config;
});
