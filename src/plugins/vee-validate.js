import Vue from 'vue';
import VeeValidate, { Validator } from 'vee-validate';
import * as es from 'vee-validate/dist/locale/es';

Vue.use(VeeValidate, {
  inject: true,
  fieldsBagName: 'vvFields',

  locale: 'es',
  events: 'change|blur|keyup',
  classes: true,
  classNames: {
    valid: 'is-valid',
    invalid: 'is-invalid',
  },
});

Validator.localize('es', es);
