export default {
  authenticate(state, payload) {
    state.access_token = payload.access_token;
    state.loggedIn = true;
    state.user = payload.user;
  },
  logout(state) {
    state.access_token = null;
    state.loggedIn = false;
    state.user = null;
  },
};
