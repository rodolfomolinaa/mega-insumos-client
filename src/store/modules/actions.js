import router from '../../router';
// import store from '../../store';

export default {
  authenticate({ commit }, payload) {
    commit('authenticate', {
      access_token: payload.access_token,
      user: payload.user,
    });
  },
  logout({ commit }) {
    commit('logout');
    router.push({ name: 'Login' });
  },
};
