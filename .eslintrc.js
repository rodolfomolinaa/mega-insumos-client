// module.exports = {
//   root: true,
//   env: {
//     node: true
//   },
//   'extends': [
//     'plugin:vue/essential',
//     'eslint:recommended'
//   ],
//   rules: {
//     'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off',
//     'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off'
//   },
//   parserOptions: {
//     parser: 'babel-eslint'
//   }
// }

module.exports = {
  env: {
    browser: true,
    es6: true,
    node: true,
  },
  extends: [
    'airbnb-base',
    'plugin:vue/recommended'
  ],
  globals: {
    Atomics: 'readonly',
    SharedArrayBuffer: 'readonly',
  },
  parserOptions: {
    ecmaVersion: 2018,
    sourceType: 'module',
    parser: 'babel-eslint'
  },
  plugins: [
    'vue',
  ],
  rules: {
    // 'vue/require-prop-type-constructor': 'error',
    // 'vue/no-unused-components': 'error',
    // 'vue/require-prop-type-constructor': 'error',
    // 'vue/no-spaces-around-equal-signs-in-attribute': 'error',
    // 'vue/no-template-shadow': 'error',
    // 'vue/singleline-html-element-content-newline': 'error',
    // 'vue/multiline-html-element-content-newline': 'error',
    // 'vue/use-v-on-exact': 'error',
    "no-new": 0,
    'import/no-extraneous-dependencies': 'off',
    'import/no-cycle': 'off',
    'no-param-reassign': [
      'error',
      {
        ignorePropertyModificationsFor: [
          'state',
        ],
      },
    ],
  },
};
